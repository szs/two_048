#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

int main(int argc, char *argv[])
{
    if (field_size != 16){
        fprintf(stderr, "ERROR: check works only with field_size = 16.\n");
        return 1;
    }

    // zeros
    int field1[] = {0,0,0,0,
                    0,0,0,0,
                    0,0,0,0,
                    0,0,0,0};
    assert(!is_playable(-1, field1));
    assert(!is_playable(0, field1));
    assert(!is_playable(1, field1));
    assert(!is_playable(2, field1));
    assert(!is_playable(3, field1));

    //playable
    int field2[] = {0,2,0,0,
                    0,0,0,0,
                    0,0,2,0,
                    0,0,0,0};
    assert(is_playable(-1, field2));
    assert(is_playable(0, field2));
    assert(is_playable(1, field2));
    assert(is_playable(2, field2));
    assert(is_playable(3, field2));

    //not playable
    int field3[] = {2,4,8, 16,
                    4,8,16,32,
                    2,4,8, 16,
                    4,8,16,32};
    assert(!is_playable(-1, field3));
    assert(!is_playable(0, field3));
    assert(!is_playable(1, field3));
    assert(!is_playable(2, field3));
    assert(!is_playable(3, field3));

     //only to right
    int field4[] = {2,0,0,0,
                    4,0,0,0,
                    2,0,0,0,
                    4,0,0,0};
    assert(is_playable(-1, field4));
    assert(!is_playable(0, field4));
    assert(!is_playable(1, field4));
    assert(is_playable(2, field4));
    assert(!is_playable(3, field4));

    return 0;
}

