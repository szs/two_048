#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

int main(int argc, char *argv[])
{
    if (row_size != 4){
        fprintf(stderr, "ERROR: check works only with row_size = 4.\n");
        return 1;
    }

    int row1[] = {0,0,0,0};
    int row1_shift[] = {0,0,0,0};
    shift_row(row1);
    assert(is_equal_row(row1, row1_shift));

    int row2[] = {0,1,0,0};
    int row2_shift[] = {1,0,0,0};
    shift_row(row2);
    assert(is_equal_row(row2, row2_shift));

    int row3[] = {0,1,1,0};
    int row3_shift[] = {2,0,0,0};
    shift_row(row3);
    assert(is_equal_row(row3, row3_shift));

    int row4[] = {1,1,2,2};
    int row4_shift[] = {2,4,0,0};
    shift_row(row4);
    assert(is_equal_row(row4, row4_shift));

    return 0;
}

