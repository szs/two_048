#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

int main(int argc, char *argv[])
{
    if (field_size != 16){
        fprintf(stderr, "ERROR: check works only with field_size = 16.\n");
        return 1;
    }

    // zeros
    int field1[] = {0,0,0,0,
                    0,0,0,0,
                    0,0,0,0,
                    0,0,0,0};
    int field1_shift[] = {0,0,0,0,
                          0,0,0,0,
                          0,0,0,0,
                          0,0,0,0};
    shift_field(0,field1);
    assert(is_equal_field(field1, field1_shift));

    //left
    int field2[] = {0,2,0,0,
                    0,0,0,0,
                    0,0,2,0,
                    0,0,0,0};
    int field2_shift[] = {2,0,0,0,
                          0,0,0,0,
                          2,0,0,0,
                          0,0,0,0};
    shift_field(0,field2);
    assert(is_equal_field(field2, field2_shift));

    //up
    int field3[] = {0,2,0,0,
                    0,0,0,0,
                    0,0,2,0,
                    0,0,0,0};
    int field3_shift[] = {0,2,2,0,
                          0,0,0,0,
                          0,0,0,0,
                          0,0,0,0};
    shift_field(1,field3);
    assert(is_equal_field(field3, field3_shift));

    //right
    int field4[] = {0,2,0,0,
                    0,0,0,0,
                    0,0,2,0,
                    0,0,0,0};
    int field4_shift[] = {0,0,0,2,
                          0,0,0,0,
                          0,0,0,2,
                          0,0,0,0};
    shift_field(2,field4);
    assert(is_equal_field(field4, field4_shift));

    //down
    int field5[] = {0,2,0,0,
                    0,0,0,0,
                    0,0,2,0,
                    0,0,0,0};
    int field5_shift[] = {0,0,0,0,
                          0,0,0,0,
                          0,0,0,0,
                          0,2,2,0};
    shift_field(3,field5);
    assert(is_equal_field(field5, field5_shift));

    //left, different number on same row
    int field6[] = {0,2,0,8,
                    0,0,0,0,
                    4,0,2,0,
                    0,0,0,0};
    int field6_shift[] = {2,8,0,0,
                          0,0,0,0,
                          4,2,0,0,
                          0,0,0,0};
    shift_field(0,field6);
    assert(is_equal_field(field6, field6_shift));

    //left, same number on same row
    int field7[] = {0,2,0,2,
                    0,0,0,0,
                    4,0,4,0,
                    0,0,0,0};
    int field7_shift[] = {4,0,0,0,
                          0,0,0,0,
                          8,0,0,0,
                          0,0,0,0};
    shift_field(0,field7);
    assert(is_equal_field(field7, field7_shift));

    return 0;
}
