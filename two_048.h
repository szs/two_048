/**
 * \file two_048.h
 * \brief Header providing principle functions for two_048
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2014 Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 */

#ifndef TWO_048_H
#define TWO_048_H

/**
 * \brief The function declaration for the player function
 *
 * This function has to be implemented and preloaded when running the program.
 * it is called on each turn. Making use of the field information, the function
 * is supposed to calculate the direction of the next shift.
 *
 * \return integer from 0 to 3, meaning:
 * 0: left
 * 1: up
 * 2: right
 * 3: down
 */
int player (const int field[]) __attribute__((weak));

/**
 * \brief Initialization function for the player
 *
 * This function is called once at the beginning of the game. It can be used
 * for variable initialisation etc.
 */
void player_init () __attribute__((weak));

/**
 * \brief The field size of the game. Usually 16. Is set in two_048_main.c
 */
static const int field_size = 16;
/**
 * \brief The field size of the game. Usually 4. Is set and asserted in
 * two_048_main.c
 */
static const int row_size = 4; // must be sqrt(field_size)!

#endif // TWO_048_H
