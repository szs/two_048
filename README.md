two_024
=======

Engine and simple test environment for the game 2084 (see 
https://en.wikipedia.org/wiki/2048_(video_game)). The idea is to implement
different "player engines" to see which one is better.

Instructions
------------

Run with 

    ./two_048 <number_of_test_runs>