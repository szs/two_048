

CC?=gcc
CFLAGS=-std=c99 -fPIC -Wall -pedantic -g -O0 -lm
#CFLAGS+= -Wextra

PLAYERS_SRC=two_048_player_random.c two_048_player_steffen1.c
PLAYERS=$(PLAYERS_SRC:.c=)

N_EXPERIMENTS=1000

MAIN_SRC=two_048_main.c
MAIN_BIN=two_048_main

CHECK_SRC=two_048_check_shift_row.c \
	two_048_check_shift_field.c \
	two_048_check_is_playable.c

CHECK_BIN=$(CHECK_SRC:.c=)

ADDITIONAL_SRC=two_048_helpers.c

default: all

all: check play


main:
	@echo -e "--- compiling main ---"
	gcc $(CFLAGS) $(MAIN_SRC) $(ADDITIONAL_SRC) -o $(MAIN_BIN)

players:
	@echo -e "--- compiling players ---"
	@for player in $(PLAYERS); do \
		cmd="gcc $(CFLAGS) -shared ./$$player.c $(ADDITIONAL_SRC) -o ./$$player.so"; \
		echo $$cmd; \
		eval $$cmd; \
		if [[ $$? -ne 0 ]]; then \
			echo -e "\033[01;31m### compiling " $$player " failed ###\033[0m\n" ; \
			exit 1; \
		fi \
	done;

play: main players
	@echo -e "--- running games ---"
	@for player in $(PLAYERS); do \
		cmd="LD_PRELOAD=./$$player.so ./$(MAIN_BIN) $(N_EXPERIMENTS)"; \
		echo $$cmd; \
		eval $$cmd || \
		echo -e "\033[01;31m### playing " $$player " failed ###\033[0m\n" ; \
	done;

compile_check:
	@echo -e "--- compiling check ---"
	@for check in $(CHECK_BIN); do \
		cmd="gcc $(CFLAGS) ./$$check.c $(ADDITIONAL_SRC) -o ./$$check"; \
		echo $$cmd; \
		eval $$cmd; \
		if [[ $$? -ne 0 ]]; then \
			echo -e "\033[01;31m### compiling " $$check " failed ###\033[0m\n" ; \
			exit 1; \
		fi \
	done;

check: compile_check
	@echo -e "--- running checks ---"
	@for check in $(CHECK_BIN); do \
		cmd="./$$check"; \
		echo $$cmd; \
		eval $$cmd; \
		if [[ $$? -ne 0 ]]; then \
			echo -e "\033[01;31m### check " $$check " failed ###\033[0m\n" ; \
			exit 1; \
		fi \
	done;
