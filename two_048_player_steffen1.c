#include <stdlib.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

void player_init ()
{
    srand(time(NULL));
}

/* return the direction of shifting:
 * -1: all directions
 * 0: left
 * 1: up
 * 2: right
 * 3: down
 */
int player (const int field[])
{
    int direction = rand()%2;
    while (!is_playable(direction, field)) direction = rand()%4;
    return direction;
}
