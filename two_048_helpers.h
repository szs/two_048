/**
 * \file two_048_helpers.h
 * \brief Header providing helper functions for two_048
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2014 Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 */

#ifndef TWO_048_HELPERS_H
#define TWO_048_HELPERS_H

/**
 * \brief Notify that no player is preloaded
 */
int player_notimpl ();

/**
 * \brief returns 2 or 4. 2 is more probable.
 */
int two_or_four();

/**
 * \brief Check whether the field is playable (shiftable) in a given direction
 *
 * \param direction
 * -1: all directions,
 * 0: left,
 * 1: up,
 * 2: right,
 * 3: down
 *
 * \param field
 *
 * \return 0 if not playable, != 0 otherwise
 */
int is_playable(int direction, const int field[]);

/**
 * \brief Initialise the field with zeros and two random nummbers generated
 * by two_or_four
 *
 * \param field to be initialised
 */
void init_field(int field[]);

/**
 * \brief Copy fields
 *
 * Copy src to target. If src is NULL, target is filled with 0's
 *
 * \param target field
 * \param source field or NULL
 */
void copy_field(int target[], const int *source);

/**
 * \brief Check whether two rows are equal
 *
 * \param row1
 * \param row2
 *
 * \return 1 if rows are equal, 0 otherwise
 */
int is_equal_row(const int row1[], const int row2[]);

/**
 * \brief Check whether two fields are equal
 *
 * \param field1
 * \param field2
 *
 * \return 1 if fields are equal, 0 otherwise
 **/
int is_equal_field(const int field1[], const int field2[]);

/**
 * \brief Shifts a row to the left, i.e. towards index 0
 *
 * \param row
 * \return points accumulated by shiftig the row
 */
int shift_row(int row[]);

/**
 * \brief Shifts the field to the desired direction
 *
 * \param direction
 * -1: all directions,
 * 0: left,
 * 1: up,
 * 2: right,
 * 3: down
 *
 * \param field
 * \return points accumulated by shiftig the field
 */
int shift_field(int direction, int field[]);

/**
 * \brief Print a row to stdout
 *
 * \param row
 */
void print_row(const int row[]);

/**
 * \brief Print the field to stdout
 *
 * \param field
 */
void print_field(const int field[]);

/**
 * \brief insert a 2 or 4 at a random field which holds a 0
 *
 * \param field
 */
void insert_random(int field[]);

/**
 * \brief Sum up all numbers in the field
 *
 * \param field
 *
 * \return integer sum of all numbers in the field
 */
int sum_field(const int field[]);

/**
 * \brief Get the largest number in the field
 *
 * \param field
 *
 * \return largest number in the field
 */
int get_max(const int field[]);

#endif // TWO_048_HELPERS_H
