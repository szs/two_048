#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

int player_notimpl ()
{
    fprintf(stderr, "Player not loaded.\n");
    return -1;
}

int two_or_four()
{
    int r = rand()%10;
    if (r < 2) return 4;
    return 2;
}

/* direction means:
 * -1: all directions
 * 0: left
 * 1: up
 * 2: right
 * 3: down
 * return value is 0 if not playable, != 0 otherwise
 */
int is_playable(int direction, const int field[])
{
    // if all numbers are 0, the field is not playable.
    // this should not happen
    if (sum_field(field) == 0) return 0;

    // if general playablility (direction == -1) should be checked
    if (direction == -1) {

        // if there is any zero in the field, it is playable
        int field_product = 1;
        for (int i=0; i<field_size; ++i) {
            field_product *= field[i];
            // avoid large numbers
            if(field_product) field_product = 1;
        }

        if (field_product == 0) return 1;

        // if there are two adjecent equal numbers in the field, it is playable
        for (int i=0; i<row_size-1; ++i) {
            for (int j=0; j<row_size-1; ++j) {
                if (field[j+i*row_size] == field[j+i*row_size + 1]) return 2;
                if (field[j+i*row_size] == field[j+(i+1)*row_size]) return 3;
            }
        }
    } else { // direction is 0,1,2 or 3
        int test_field[field_size];
        copy_field(test_field, field);
        shift_field(direction, test_field);
        if (!is_equal_field(test_field, field)) return 4;
    }


    return 0;
}

// returns 1 if rows are equal, 0 otherwise
int is_equal_row(const int row1[], const int row2[])
{
    for (int i=0; i<row_size; ++i) {
        if (row1[i] != row2[i]) return 0;
    }

    return 1;
}

// returns 1 if fields are equal, 0 otherwise
int is_equal_field(const int field1[], const int field2[])
{
    for (int i=0; i<field_size; ++i) {
        if (field1[i] != field2[i]) return 0;
    }

    return 1;
}

void init_field(int field[])
{
    for (int i=0; i < field_size; ++i) field[i] = 0;
    field[rand()%field_size] = two_or_four();
    field[rand()%field_size] = two_or_four();
}

void copy_field(int target[], const int *source)
{
    if (source != NULL) {
        for (int i=0; i < field_size; ++i) target[i] = source[i];
    } else {
        for (int i=0; i < field_size; ++i) target[i] = 0;
    }
}

int shift_row(int row[])
{
    // check whether all numbers in row are zero
    int row_sum=0;
    for (int i=0; i < row_size; ++i) {
        row_sum += row[i];
    }

    // only shift numbers if not all numbers are zero
    if (row_sum == 0) return 0;

    //shift if number to the left of the current number is zero
    for (int j=1; j<row_size; ++j) {
        if (row[j] != 0 && row[j-1] == 0) {
            for (int i=j; i < row_size; ++i) {
                row[i-1] = row[i];
            }
            row[row_size-1] = 0;
            if (j > 1) j-=2;
        }
    }

    int points = 0;
    // sum equal numbers
    for (int i=0; i < row_size-1; ++i) {
        if (row[i] != 0 && row[i] == row[i+1]){
            row[i] *= 2;
            points = row[i];
            for (int j=i+1; j<row_size-1; ++j) {
                row[j] = row[j+1];
            }
            row[row_size-1] = 0;
        }
    }
    return points;
}

int shift_field(int direction, int field[])
{
    int row[4];
    int points = 0;

    for (int i=0; i < row_size; ++i) {
        for (int j=0; j < row_size; ++j) {
            switch (direction) {
                case 0: // left
                    row[j] = field[i*row_size+j];
                    break;
                case 1: // up
                    row[j] = field[j*row_size+i];
                    break;
                case 2: // right
                    row[j] = field[(i+1)*row_size-j-1];
                    break;
                case 3: // down
                    row[j] = field[row_size*(row_size-1)+i-row_size*j];
                    break;
                default: // not allowed
                    fprintf(stderr,"ERROR: direction %d not allowed!", direction);
            }
        }

        points += shift_row(row);

        for (int j=0; j < row_size; ++j) {
            switch (direction) {
                case 0: // left
                    field[i*row_size+j] = row[j];
                    break;
                case 1: // up
                    field[j*row_size+i] = row[j];
                    break;
                case 2: // right
                    field[(i+1)*row_size-j-1] = row[j];
                    break;
                case 3: // down
                    field[row_size*(row_size-1)+i-row_size*j] = row[j];
                    break;
                default: // not allowed
                    fprintf(stderr,"ERROR: direction %d not allowed!", direction);
            }
        }
    }
    return points;
}


void print_row(const int row[])
{
    for (int i=0; i<row_size; ++i) {
        fprintf(stdout, "%d\t", row[i]);
    }
    fprintf(stdout, "\n");
}

void print_field(const int field[])
{
    for (int i=0; i<row_size; ++i) {
        print_row(field + i * row_size);
        //fprintf(stdout, "\n");
    }
    fprintf(stdout, "-\n\n");
}

void insert_random(int field[])
{
    int count = 0;
    for (int i=0; i<field_size; ++i) {
        if (field[i] == 0) ++count;
    }
    if (count == 0) return;
    int rnd = rand() % count;

    for (int i=0; i<field_size; ++i) {
        if (field[i] == 0 && rnd-- == 0) {
            field[i] = two_or_four();
            return;
        }
    }
}

int sum_field(const int field[])
{
    int field_sum = 0;
    for (int i=0; i<field_size; ++i) {
        field_sum += field[i];
    }
    return field_sum;
}

int get_max(const int field[])
{
    int field_max = 0;
    for (int i=0; i<field_size; ++i) {
        if (field[i] > field_max) field_max = field[i];
    }
    return field_max;
}
