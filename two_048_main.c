#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "two_048.h"
#include "two_048_helpers.h"

int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "usage: two_048 <number_of_runs>\n");
        return 1;
    }

    if (!player) {
        player_notimpl();
        return 2;
    }

    assert(row_size*row_size == field_size);

    int num_exp = atoi(argv[1]);

    player_init();

    srand(time(NULL));

    int field[field_size];

    double arith_mean_result = 0;
    double geo_mean_result = 1.0;
    double game_points = 0.0;
    int invalid_move_count = 0;
    int invalid_game_flag = 0;
    int invalid_game_count = 0;

    for (int i=0; i < num_exp; ++i) {
        init_field(field);
        game_points = 0;
        //print_field(field);
        while (is_playable(-1, field)) {
            // player input
            int direction = player(field);
            // check for validity of move
            if ( direction < 0 || direction > 3 ) ++invalid_move_count;
            if ( ! is_playable(direction, field) ) ++invalid_move_count;
            if ( invalid_move_count > 10 ) {
                invalid_game_flag = 1;
                break;
            }

            // perform turn
            game_points += shift_field(direction, field);
            insert_random(field);
            //print_field(field);
        }

        if ( invalid_game_flag == 1 ) {
            // (re)set counters and flags
            invalid_game_flag = 0;
            invalid_move_count = 0;
            ++invalid_game_count;
            // do not count points
            game_points = 0;
            continue;
        } else {
            // calculate score
            //game_points = (double) (sum_field(field) + get_max(field));
        }

        // calculate means
        arith_mean_result += game_points / num_exp;
        geo_mean_result *= pow(game_points, 1.0/num_exp);
    }

    printf("arithmetic mean:\t%f\n", arith_mean_result);
    printf("geometric mean: \t%f\n", geo_mean_result);
    printf("invalid games:  \t%d\n", invalid_game_count);

    return 0;
}

